/*
 * imx_rpmsg_sensor.h - header file supporting imx_rpmsg_sensor module
 */

#ifndef IMX_RPMSG_SENSOR_H
#define IMX_RPMSG_SENSOR_H

#include <linux/ioctl.h>

#define VERSION_STR	"1.2"

#define RPMSG_SENSOR_DEVICE_NAME	"imx_rpmsg_sensor"


typedef union sensor_value {
	struct sens_data {
		uint16_t	xval;
		uint16_t	yval;
		uint16_t	zval;
	}data;
	uint16_t val[3];
}sensor_value_t;

/* Sensor Data Packet format  */
typedef struct cm4_sensor_data {
	uint32_t timestamp; /* os timer tick value when sensor read */
	sensor_value_t gyro;
	sensor_value_t magn;
	sensor_value_t accel;
}cm4_sensor_data_t;

/* Local storage of sensor data format */
#define MAX_SENSOR_RNG	250
typedef struct cm4_sensors {
	uint32_t index;		/* current location in ring buffer */
	cm4_sensor_data_t sensor[MAX_SENSOR_RNG];
}cm4_sensors_t;


#ifndef IMX_SENSOR_MAJOR
#define IMX_SENSOR_MAJOR 0	/* dynamic major */
#endif

#ifndef IMX_SENSOR_NR_DEVS
#define IMX_SENSOR_NR_DEVS	1 /* imxsensor0 */
#endif

typedef struct imx_sensor_dev {
	cm4_sensors_t	m4dev;
}imx_sensor_dev_t;


#define IMX_SENSOR_IOC_MAGIC	248

#define IOCTL_RPMSG_SENSOR_GDATA	_IOR(IMX_SENSOR_IOC_MAGIC, 1, struct imx_sensor_dev)



#endif	/* IMX_RPMSG_SENSOR_H */
