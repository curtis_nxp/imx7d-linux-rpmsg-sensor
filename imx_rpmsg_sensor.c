/*
 * Copyright (C) 2016 NXP Semiconductor, Inc.
 *
 * derived from imx_rpmsg_sensor implementation.
 * Remote processor messaging transport - sensor driver
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/virtio.h>
#include <linux/rpmsg.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>

// #define DEBUG 1
#include <linux/device.h>

#include "imx_rpmsg_sensor.h"	/* local definitions */


int imx_sensor_major =	IMX_SENSOR_MAJOR;
int imx_sensor_minor =	0;
int imx_sensor_nr_devs = IMX_SENSOR_NR_DEVS;
int imx_sensor_debug = 0;	/* set =1 to turn on prints of rx pkts from m4 */

module_param(imx_sensor_major, int, S_IRUGO);
module_param(imx_sensor_nr_devs, int, S_IRUGO);
module_param(imx_sensor_debug, int, S_IRUGO);

static struct imx_sensor_dev * imx_sensor_device = 0;
static struct cdev rpmsg_sensor_cdev;
static struct class *rpmsg_sensor_class = NULL;

#define MSG		"C-A7 RPMSG ready!"
static unsigned int rpmsg_sensor;

static int rpmsg_sensor_open (struct inode * inode, struct file *filp);

static long rpmsg_sensor_ioctl (struct file *file, unsigned int cmd, unsigned long arg)
{
	int err = -EINVAL;
	switch (cmd) {
	case IOCTL_RPMSG_SENSOR_GDATA:
		if (copy_to_user ((void __user *)arg, imx_sensor_device,
				  sizeof (struct imx_sensor_dev))) {
			err = -EFAULT;
		}
		else {
			err = 0;
		}
		break;
	default:
		pr_err("rpmsg_sensor_ioctl: unknown IOCTL: %d\n", cmd);
		err = -EINVAL;
		break;
	}
	
	return err;
}
static const struct file_operations rpmsg_sensor_fops = {
	.owner = THIS_MODULE,
	.open  = rpmsg_sensor_open,
	.unlocked_ioctl = rpmsg_sensor_ioctl,
};


static int rpmsg_sensor_open (struct inode * inode, struct file *filp)
{
	const int minor = iminor(inode);
	
	if (minor == 0) {
		filp->f_op = &rpmsg_sensor_fops;
	}
	return 0;
}


static void rpmsg_sensor_cb(struct rpmsg_channel *rpdev, void *data, int len,
			    void *priv, u32 src)
{
	int err;
	cm4_sensor_data_t * pM4data;
	imx_sensor_dev_t * pM4Dev;

	/* copy sensor data to local storage */


	/* init pointer to local storage */
	pM4Dev = (imx_sensor_dev_t *) imx_sensor_device;

	/* determine where index pointer is,
	 * first entry on initialization is skipped,
	 * but entry written on wrap
	 */
	if (pM4Dev->m4dev.index == MAX_SENSOR_RNG-1) {
		/* end of the rng buffer, set to first entry */
		pM4Dev->m4dev.index = 0;
	}
	else {
		/* increment entry pointer */
		pM4Dev->m4dev.index++;
	}

	/* init pointer to c-m4 message packet */
	pM4data = (cm4_sensor_data_t *)data;

	/* copy information */
	memcpy (&pM4Dev->m4dev.sensor[pM4Dev->m4dev.index], pM4data,
		sizeof (struct cm4_sensor_data));

	if (imx_sensor_debug) {
		pr_info ("%08x %04x %04x %04x  %04x %04x %04x  %04x %04x %04x\n",
			 pM4data->timestamp, pM4data->accel.data.xval,
			 pM4data->accel.data.yval, pM4data->accel.data.zval,
			 pM4data->magn.data.xval, pM4data->magn.data.yval,
			 pM4data->magn.data.zval, pM4data->gyro.data.xval,
			 pM4data->gyro.data.yval, pM4data->gyro.data.zval);
	}
	
	/* send acknowledge packet received, return timestamp */
	err = rpmsg_sendto(rpdev, (void *)(&pM4data->timestamp), 4, src);

	if (err)
		dev_err(&rpdev->dev, "rpmsg_send failed: %d\n", err);

}

static int rpmsg_sensor_probe(struct rpmsg_channel *rpdev)
{
	int err;

	pr_info ("imx_rpmsg_sensor: probe()\n");
	
	dev_info(&rpdev->dev, "new channel: 0x%x -> 0x%x!\n",
			rpdev->src, rpdev->dst);

	/*
	 * send a message to our remote processor, and tell remote
	 * processor about this channel
	 */
	err = rpmsg_send(rpdev, MSG, strlen(MSG));
	if (err) {
		dev_err(&rpdev->dev, "rpmsg_send failed: %d\n", err);
		return err;
	}

	rpmsg_sensor = 0;

	err = rpmsg_sendto(rpdev, (void *)(&rpmsg_sensor), 4, rpdev->dst);
	if (err) {
		dev_err(&rpdev->dev, "rpmsg_send failed: %d\n", err);
		return err;
	}

	return 0;
}

static void rpmsg_sensor_remove(struct rpmsg_channel *rpdev)
{
	dev_info(&rpdev->dev, "rpmsg pingpong driver is removed\n");
}

static struct rpmsg_device_id rpmsg_driver_pingpong_id_table[] = {
	{ .name	= "rpmsg-openamp-demo-channel" },
	{ },
};
MODULE_DEVICE_TABLE(rpmsg, rpmsg_driver_pingpong_id_table);

static struct rpmsg_driver rpmsg_sensor_driver = {
	.drv.name	= KBUILD_MODNAME,
	.drv.owner	= THIS_MODULE,
	.id_table	= rpmsg_driver_pingpong_id_table,
	.probe		= rpmsg_sensor_probe,
	.callback	= rpmsg_sensor_cb,
	.remove		= rpmsg_sensor_remove,
};

static int __init rpmsg_sensor_init(void)
{
	int result = 0;
	dev_t dev = 0;
	struct device *device = NULL;

	pr_info ("imx_rpmsg_sensor: VERSION %s\n", VERSION_STR);

	/* rpmsg registeration */
	result = register_rpmsg_driver(&rpmsg_sensor_driver);
	if (result < 0)
		return result;

	if (imx_sensor_major) {
		dev = MKDEV(imx_sensor_major, imx_sensor_minor);
		result = register_chrdev_region(dev, imx_sensor_nr_devs, "imx_msg_sensor");
	} else {
		result = alloc_chrdev_region(&dev, imx_sensor_minor, imx_sensor_nr_devs,
					     RPMSG_SENSOR_DEVICE_NAME);
		imx_sensor_major = MAJOR(dev);
	}
	if (result < 0 ) {
		pr_warn ("imx_msg_sensor cannot get major %d\n", imx_sensor_major);
		return result;
	}

	/*
	 * allocate device and init with zeros
	 */
	imx_sensor_device = kzalloc (imx_sensor_nr_devs * sizeof (struct imx_sensor_dev),
				      GFP_KERNEL);
	if (!imx_sensor_device) {
		result = -ENOMEM;
		goto fail;
	}

	dev = MKDEV(imx_sensor_major, imx_sensor_minor);
	cdev_init (&rpmsg_sensor_cdev, &rpmsg_sensor_fops);
	result = cdev_add (&rpmsg_sensor_cdev, dev, imx_sensor_nr_devs);
	if (result) {
		goto fail;
	}


	if ((rpmsg_sensor_class = class_create (THIS_MODULE,
						RPMSG_SENSOR_DEVICE_NAME)) == NULL) {
		cdev_del (&rpmsg_sensor_cdev);
		result = -ENOMEM;
		goto fail;
	}
		
	/*
	 * create device
	 */
	device = device_create (rpmsg_sensor_class, NULL, /* no parent */
				dev, NULL,		  /* no more data */
				RPMSG_SENSOR_DEVICE_NAME);
	if (IS_ERR(device)) {
		result = PTR_ERR(device);
		pr_warn ("imx_rpmsg_sensor: device_create fail error %d\n",
			 result);
		cdev_del (&rpmsg_sensor_cdev);
		result = -ENOMEM;
		goto fail;
	}
		
	return 0;

 fail:
	if (imx_sensor_device)
		kfree(imx_sensor_device);

	unregister_chrdev_region (dev, imx_sensor_nr_devs);

	return result;
}

static void __exit rpmsg_sensor_fini(void)
{
	unregister_rpmsg_driver(&rpmsg_sensor_driver);

	if (imx_sensor_device)
		kfree(imx_sensor_device);

	unregister_chrdev_region (MKDEV(imx_sensor_major, imx_sensor_minor), imx_sensor_nr_devs);
}
module_init(rpmsg_sensor_init);
module_exit(rpmsg_sensor_fini);

MODULE_AUTHOR("NXP Semiconductor, Inc.");
MODULE_DESCRIPTION("iMX virtio remote processor messaging sensor driver");
MODULE_LICENSE("GPL v2");
